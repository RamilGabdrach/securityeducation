package interfaces;

import java.math.BigInteger;

public interface PrimeGenerationListner {
	/**
	 * 
	 * @param threadIndex
	 * @param iterationNumber
	 * @param testNumber
	 * @param result
	 */
	public void OnResultComplete(int threadIndex, int iterationNumber, BigInteger testNumber, boolean result);
}
