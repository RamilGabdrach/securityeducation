package interfaces;

import java.math.BigInteger;

public interface RSAParamsGeneration {
	
	public void OnPGenComplete();
	public void OnQGenComplete();
	public void OnNGenComplete();
	public void OnFiGenComplete();
	public void OnEGenComplete();
	public void OnDGenComplete();
	
}
