package hack;

import java.math.BigInteger;

public interface HackingStatus {
	public void OnHackComplete(double time, BigInteger P, BigInteger Q, BigInteger D, String message);
}
