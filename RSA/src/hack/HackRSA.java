package hack;

import java.math.BigInteger;
import java.util.ArrayList;

import utils.EulerTotientFunction;
import utils.ExtendedEuclideanAlgorithm;
import utils.RSA;
import utils.RSAParams;

public class HackRSA {

	public static int maxThread = 2;
	private static ArrayList<PolardThread> threads = new ArrayList<HackRSA.PolardThread>();
	
	public void execute(final BigInteger N, final BigInteger E, final String encMessage, final HackingStatus listner)
	{
		
		BigInteger k = new BigInteger("2");
		BigInteger a = new BigInteger("1");
		
		for(int i = 0; i < maxThread; i ++)
		{
			System.out.println("Start thread. A= " + a.toString() + " Xo= " +k.toString());
			
			PolardThread polard = new PolardThread(k, a, N, E, encMessage, listner);
			threads.add(polard);
			if(i % 2 != 0)
			{
				a = a.abs();
				a = a.add(BigInteger.ONE);
				
				k = k.add(BigInteger.ONE);
				
			}
			else
			{
				a = a.negate();
			}
			
		}
		
		for(int i = 0; i < maxThread; i++)
		{
			Thread t= new Thread(threads.get(i));
		
			t.start();
		}
		
		
		
	}
	
	public static void stopAll()
	{
		if(threads != null)
		{
			for(int i = 0; i < threads.size(); i++)
			{
				threads.get(i).stop = true;
			}
		}
	}
	
	
	
	public class PolardThread implements Runnable
	{
		public boolean stop = false;
		
		private BigInteger startX;
		
		BigInteger N,E;
		String encMessage; 
		HackingStatus listner;
		BigInteger a;
		
		public PolardThread(BigInteger startFrom ,BigInteger _a, final BigInteger _N, final BigInteger _E, final String _encMessage, final HackingStatus _listner)
		{
			startX = startFrom;
			a = _a;
			N = _N;
			E = _E;
			encMessage = _encMessage;
			listner = _listner;
		}
		
		@Override
		public void run() {
			long beginTime = System.currentTimeMillis();
			
			BigInteger i = new BigInteger("1");
			
			BigInteger x = startX;
			BigInteger y = new BigInteger("1");
			
			BigInteger d = (x.subtract(y)).abs();
			BigInteger t = N.gcd(d);
			
			BigInteger twoStep = new BigInteger("2");
			BigInteger two = new BigInteger("2");
			
			while(!stop && t.compareTo(BigInteger.ONE) == 0)
			{
				//System.out.println("Iteration number = " + i.toString());
				if(i.compareTo(twoStep) == 0)
				{
					y = x;
					twoStep = twoStep.multiply(two);
					System.out.println("Iteration number = " + i.toString());
				}
				x = ((x.pow(2)).subtract(a)).mod(N);
				//x = ((x.multiply(x)).subtract(a)).mod(N);
				//x = ((x.modPow(two, N)).subtract(a)).mod(N);
				
				
				i = i.add(BigInteger.ONE);
				
				//System.out.println("x = " + x.toString());
				
				d = (x.subtract(y)).abs();
				t = N.gcd(d);
			}
			if(!stop)
			{
				HackRSA.stopAll();
				
				System.out.println("find for i = " + i.toString() +" iteration");
				
				BigInteger p = t;
				BigInteger q = N.divide(p);
				
				BigInteger fi = EulerTotientFunction.get(p, q);
				
				d = ExtendedEuclideanAlgorithm.exicute(E, fi);
				
				RSAParams  param = new RSAParams(123);
				param.D = d;
				param.E = E;
				param.Fi = fi;
				param.N = N;
				param.P = p;
				param.Q = q;
				
				String mes = RSA.decode(param, encMessage);
				
				
				long endTime = System.currentTimeMillis();
				
				double time = (endTime - beginTime)/1000d;
				
				listner.OnHackComplete(time, p, q, d, mes);
				
			}
			
		}
		
	}
}
