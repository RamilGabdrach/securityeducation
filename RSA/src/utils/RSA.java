package utils;

import java.math.BigInteger;



//1105 = ' '
//1106 = '.'
public class RSA {
	
	public static String encrypt(RSAParams param, String message)
	{
		BigInteger m = BigInteger.ZERO;
		BigInteger temp = BigInteger.ONE;
		
		BigInteger sto = new BigInteger("100");
		for(int i = message.length() -1 ; i >= 0 ; i --)
		{
			Integer symbol = (int) message.charAt(i) - 1024;
			
			if(message.charAt(i) == ' ')
				symbol = 83;
			
			if(message.charAt(i) == '.')
				symbol = 84;
			
			m = m.add(temp.multiply(new BigInteger(symbol.toString())));
			
			temp = temp.multiply(sto);
		}
		BigInteger c = m.modPow(param.E, param.N);
		return  c.toString();
	}
	
	public static String decode(RSAParams param, String cMes)
	{
		BigInteger c = new BigInteger(cMes);
		
		
		BigInteger m = c.modPow(param.D, param.N);
		
		BigInteger temp = BigInteger.ONE;
		
		String message = "";
		BigInteger sto = new BigInteger("100");
		
		while(m.compareTo(BigInteger.ZERO) > 0)
		{
			BigInteger devResult[] = m.divideAndRemainder(sto);
			
			int s = devResult[1].intValue() + 1024;
			char symbol  = (char) s; 
			
			if(s - 1024 == 83)
				symbol = ' ';
			if(s - 1024 == 84)
				symbol = '.';
			
			message =  symbol + message;
			
			m = devResult[0];
		}
		
		
		
		return message;
	}

}
