package utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class ExtendedEuclideanAlgorithm {

	public static BigInteger  exicute(BigInteger a, BigInteger b)
	{
		
		if(a.equals(BigInteger.ONE))
			return a;
		
		ArrayList<BigInteger> arrayOfK = defineArrayOfK(a,b);
		
		//i-2, i-1, i
		ArrayList<BigInteger> S = new ArrayList<BigInteger>(3);
		for(int i = 0; i < 3; i ++)
		{
			S.add(BigInteger.ONE);
		}
		
		S.set(1, BigInteger.ONE);
	
		
		S.set(2, arrayOfK.get(1).negate());
		
		for(int i = 2; i < arrayOfK.size()-1; i ++)
		{
			shift(S);
			S.set(2, S.get(0).subtract(arrayOfK.get(i).multiply(S.get(1))));
		}
		
		BigInteger result = S.get(2);
		if(result.signum() == 1)
		{
			if(result.compareTo(b) >= 0)
				result.mod(b);
		}
		else
		{
			while(result.signum() == -1)
				result = result.add(b);
			
			result.mod(b);
		}
		
		return result;
	}

	private static void shift(ArrayList<BigInteger> s) {
		s.set(0, s.get(1));
		s.set(1, s.get(2));
	}

	private static ArrayList<BigInteger> defineArrayOfK(BigInteger a, BigInteger b) {
		
		ArrayList<BigInteger> result = new ArrayList<BigInteger>();
		
		//k0
		BigInteger[] devResult = a.divideAndRemainder(b);
		result.add(devResult[0]);
		BigInteger prev = devResult[1];
		
		//k1
		devResult = b.divideAndRemainder(devResult[1]);
		result.add(devResult[0]);

		
		while(devResult[1].compareTo(BigInteger.ZERO) != 0)
		{
			BigInteger temp = devResult[1];
			devResult = prev.divideAndRemainder(devResult[1]);
			result.add(devResult[0]);
			prev = temp;
		}
		
		return result;
	}

}
