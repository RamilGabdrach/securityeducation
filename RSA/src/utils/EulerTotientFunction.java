package utils;

import java.math.BigInteger;

public class EulerTotientFunction {

	public static BigInteger get(BigInteger p, BigInteger q) {
		return p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
	}

}
