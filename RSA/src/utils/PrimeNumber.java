package utils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Random;

public class PrimeNumber {
	
	
	private static ArrayList<BigInteger> firstPrime;
	
	static{
		firstPrime = new ArrayList<BigInteger>();
		firstPrime.add(new BigInteger("5"));
		firstPrime.add(new BigInteger("7"));
		firstPrime.add(new BigInteger("11"));
		firstPrime.add(new BigInteger("13"));
		firstPrime.add(new BigInteger("17"));
		firstPrime.add(new BigInteger("19"));
		firstPrime.add(new BigInteger("23"));
		firstPrime.add(new BigInteger("29"));
		firstPrime.add(new BigInteger("31"));
		firstPrime.add(new BigInteger("37"));
	}
	
	public static BigInteger getPrime()
	{
		return  getPrime(64);
	}
	
	public static BigInteger getPrime(int length)
	{
		long temp = System.currentTimeMillis();
		
		BigInteger prime;
		BigInteger two = new BigInteger("2");
		
		do
		{
			do
			{
				prime = getRandomNumber(length);
			}
			while(prime.signum() != 1);
			
			if(prime.remainder(two).compareTo(BigInteger.ZERO) == 0)
				prime = prime.add(BigInteger.ONE);
			
			//System.out.println("Test " + prime + " on prime");
		}
		while(!MillerRobinTest(prime, 8));
		
		double time = (System.currentTimeMillis()-temp)/1000d;
		System.out.println("Prime with length "+ length +" gen in " + time + " sec.");
		
		return prime;
	}
	
	
	
	private static BigInteger getRandomNumber(int length)
	{
		return new BigInteger(length, new Random());
	}
	
	public static boolean MillerRobinTest(BigInteger n, int rounds)
	{
		if(n.signum() == -1)
			System.out.println("n = " + n);
		
		
		BigInteger k,temp, t = null;
		
		temp = n.subtract(BigInteger.ONE);
		
		k = BigInteger.ZERO;
		
		BigInteger two = new BigInteger("2");
		
		boolean devided = true;
		while(temp.compareTo(BigInteger.ZERO) > 0 && devided)
		{
			BigInteger r[] = temp.divideAndRemainder(two);
			if(r[1].compareTo(BigInteger.ZERO) == 0)
			{
				k = k.add(BigInteger.ONE);
				temp = r[0];
			}
			else
			{
				devided = false;
				t = temp;
			}
			
		}
		
		temp = n.subtract(BigInteger.ONE);
		
		
		boolean isPrime = true;
		int i = 0;
		while(isPrime && i < rounds)
		{
			BigInteger a = firstPrime.get(i);
			BigInteger b = a.modPow(t, n);
			
			
			
			
			if(b.compareTo(BigInteger.ONE) == 0 || b.compareTo(temp) == 0)
			{
				i++;
				continue;
			}
			
			isPrime = false;
			BigInteger counter = k.subtract(BigInteger.ONE);
			while(!isPrime && counter.compareTo(BigInteger.ZERO) != 0 && counter.signum() == 1)
			{
				b = b.modPow(two, n);
				
				if(b.equals(temp))
					isPrime = true;
				
				counter = counter.subtract(BigInteger.ONE);
			}
			
			i++;
		}
		
		
		return isPrime;
	}
	
	@Deprecated
	public static BigInteger gen512PrimeNumber()
	{
		BigInteger p = getPrime(64);
		BigInteger two = new BigInteger("2");
		
		for(int i = 0; i < 4; i++)
		{
			
			BigInteger t = (p.multiply(two));
			
			t = t.add(two);
	
			BigInteger q;
			int j = 0;
			
			do{
				j++;
		
				t = t.subtract(two);
			
				q = (p.multiply(t)).add(BigInteger.ONE);
			}	
			while(!MillerRobinTest(q, 4));
			
			System.out.println("count = " + j);
			p = q;
		}
		
		return p;
		
	}

}
