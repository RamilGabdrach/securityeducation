package utils;

import interfaces.PrimeDefindListner;
import interfaces.RSAParamsGeneration;

import java.math.BigInteger;
import java.util.Random;

import parallel.utils.ParallelPrimeNumber;


public class RSAParams implements PrimeDefindListner {

	public BigInteger P;
	public BigInteger Q;
	public BigInteger N;
	public BigInteger Fi;
	
	public BigInteger E;
	public BigInteger D;
	
	private int strength;
	private RSAParamsGeneration listner;
	
	public RSAParams(int strength, RSAParamsGeneration _listner)
	{
		this(strength);
		listner = _listner;
	}
	
	public RSAParams(int _strength)
	{
		strength = _strength;
	}
	
	public void generete()
	{
		
		if(strength <= 64)
		{
			System.out.println("Line RSA Params Generation");
			lineGeneration();
		}
		else
		{
			System.out.println("Parallel RSA Params Generation");
			parallelGeneration();
		}
			
		
	
	}
	
	
	public void lineGeneration()
	{
		P = PrimeNumber.getPrime(strength);
		if(listner != null)
			listner.OnPGenComplete();
		
		Q = PrimeNumber.getPrime(strength);
		if(listner != null)
			listner.OnQGenComplete();
	
		N = P.multiply(Q);
		
		if(listner != null)
			listner.OnNGenComplete();
		
		Fi = EulerTotientFunction.get(P, Q);
		if(listner != null)
			listner.OnFiGenComplete();
		
		
		generateE();
		if(listner != null)
			listner.OnEGenComplete();
		generateD();
		if(listner != null)
			listner.OnDGenComplete();
		
	}
	public void parallelGeneration()
	{
		new ParallelPrimeNumber().generatePrime(strength, this);
	}
	
	

	private void generateD() {
		D = ExtendedEuclideanAlgorithm.exicute(E, Fi);
	}


	private void generateE() {
		E =  new BigInteger(strength/3, new Random());
		
		while(E.gcd(Fi).compareTo(BigInteger.ONE) != 0)
		{
			E = E.add(BigInteger.ONE);
		}
		
	}
	
	public String toString()
	{
		return "P= " + P + "\nQ= " + Q + "\nE= " + E;
	}

	@Override
	public void OnDefind(BigInteger prime) {
		if(P == null)
		{
			P = prime;
			if(listner != null)
				listner.OnPGenComplete();
			
			new ParallelPrimeNumber().generatePrime(strength,this);
		}
		else
		{
			Q = prime;
			if(P.compareTo(Q) == 0)
			{
				new ParallelPrimeNumber().generatePrime(strength,this);
			}
			else
			{
				if(listner != null)
					listner.OnQGenComplete();
				
				
				
				N = P.multiply(Q);
				
				if(listner != null)
					listner.OnNGenComplete();
				
				Fi = EulerTotientFunction.get(P, Q);
				if(listner != null)
					listner.OnFiGenComplete();
				
				
				generateE();
				if(listner != null)
					listner.OnEGenComplete();
				generateD();
				if(listner != null)
					listner.OnDGenComplete();
			}
				
			
		}
		
		
	}
}
