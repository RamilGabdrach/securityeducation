package parallel.utils;

import interfaces.PrimeDefindListner;
import interfaces.PrimeGenerationListner;

import java.math.BigInteger;
import java.util.ArrayList;

import utils.PrimeNumber;

public class ParallelPrimeNumber implements PrimeGenerationListner {
	
	
	private int MaxThreadNumbers = 2;
	
	private ArrayList<ParallelMillerRabinTest> threads;
	
	BigInteger two = new BigInteger("2");
	BigInteger p;
	BigInteger t;
	
	boolean complete = false;
	
	int iterationNumber = 0;
	
	PrimeDefindListner listner;
	public void generatePrime(int sthength, PrimeDefindListner l)
	{
		listner = l;
		p = PrimeNumber.getPrime(sthength/8);
		
		startRound();
	}
	
	public void generatePrime(PrimeDefindListner l)
	{
		generatePrime(64,l);
	}
	
	public BigInteger generatePrime()
	{
		p = PrimeNumber.getPrime(64);
		
		startRound();
		
		while(!complete)
		{
			
		}
			
			
		return p;
	}
	
	private void startRound()
	{
		if(iterationNumber < 4)
		{
			ArrayList<Thread> th = new ArrayList<Thread>();
			
			threads = new ArrayList<ParallelMillerRabinTest>();
			
			t = (p.multiply(two));
			t = t.add(two);
	
			for(int i= 0; i < MaxThreadNumbers; i ++)
			{
				t = t.subtract(two);
				BigInteger q = (p.multiply(t)).add(BigInteger.ONE);
				ParallelMillerRabinTest r = new ParallelMillerRabinTest(q, 4, iterationNumber, i, this);
				
				threads.add(r);
				th.add(new Thread(r));
			}
			
			for(int i= 0; i < MaxThreadNumbers; i ++)
			{
				th.get(i).start();
			}
		}
		else
		{
			complete = true;
			if(listner!= null)
				listner.OnDefind(p);
		}
	}
	

	@Override
	public void OnResultComplete(int threadIndex, int _iterationNumber, BigInteger testNumber, boolean result) {
		if(iterationNumber == _iterationNumber)
		{
			if(result)
			{
				iterationNumber++;
				for(int i = 0; i < MaxThreadNumbers; i++)
					threads.get(i).stopThread();
				
				p = testNumber;
				startRound();
			}
			else
			{
				t = t.subtract(two);
				BigInteger q = (p.multiply(t)).add(BigInteger.ONE);
				ParallelMillerRabinTest r = new ParallelMillerRabinTest(q, 4, iterationNumber, threadIndex, this);
				
				threads.add(r);
				
				Thread t = new Thread(r);
				t.start();
			}
		}
		
	}
}
