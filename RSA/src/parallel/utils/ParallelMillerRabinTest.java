package parallel.utils;

import java.math.BigInteger;
import java.util.ArrayList;

import interfaces.PrimeGenerationListner;

public class ParallelMillerRabinTest implements Runnable{

	private PrimeGenerationListner listner;
	private BigInteger n;
	private int rounds;
	private int iterationNumber;
	private int threadIndex;
	
	private boolean stop = false;
	
	
	private ArrayList<BigInteger> firstPrime;
	
	
	public ParallelMillerRabinTest(BigInteger testNumber, int _rounds, int _iterationNumber, int _threadIndex, PrimeGenerationListner _listner)
	{
		listner = _listner;
		n = testNumber;
		rounds = _rounds;
		iterationNumber = _iterationNumber;
		threadIndex = _threadIndex;
		
		firstPrime = new ArrayList<BigInteger>();
		firstPrime.add(new BigInteger("5"));
		firstPrime.add(new BigInteger("7"));
		firstPrime.add(new BigInteger("11"));
		firstPrime.add(new BigInteger("13"));
		firstPrime.add(new BigInteger("17"));
		firstPrime.add(new BigInteger("19"));
		firstPrime.add(new BigInteger("23"));
		firstPrime.add(new BigInteger("29"));
		firstPrime.add(new BigInteger("31"));
		firstPrime.add(new BigInteger("37"));
	}
	
	public void stopThread()
	{
		stop = true;
	}
	
	@Override
	public void run() {
		
		BigInteger k,temp, t = null;
		
		temp = n.subtract(BigInteger.ONE);
		
		k = BigInteger.ZERO;
		
		BigInteger two = new BigInteger("2");
		
		boolean devided = true;
		while(!stop && temp.compareTo(BigInteger.ZERO) > 0 && devided)
		{
			BigInteger r[] = temp.divideAndRemainder(two);
			if(r[1].compareTo(BigInteger.ZERO) == 0)
			{
				k = k.add(BigInteger.ONE);
				temp = r[0];
			}
			else
			{
				devided = false;
				t = temp;
			}
			
		}
		
		temp = n.subtract(BigInteger.ONE);
		
		
		boolean isPrime = true;
		int i = 0;
		while(!stop && isPrime && i < rounds)
		{
			BigInteger a = firstPrime.get(i);
			BigInteger b = a.modPow(t, n);
			
			
			
			
			if(b.compareTo(BigInteger.ONE) == 0 || b.compareTo(temp) == 0)
			{
				i++;
				continue;
			}
			
			isPrime = false;
			BigInteger counter = k.subtract(BigInteger.ONE);
			while(!stop && !isPrime && counter.compareTo(BigInteger.ZERO) != 0 && counter.signum() == 1)
			{
				b = b.modPow(two, n);
				
				if(b.equals(temp))
					isPrime = true;
				
				counter = counter.subtract(BigInteger.ONE);
			}
			
			i++;
		}
		
		listner.OnResultComplete(threadIndex, iterationNumber, n, isPrime);
	}

}
