package view;

import interfaces.RSAParamsGeneration;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;

import utils.RSAParams;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.math.BigInteger;
import javax.swing.JTextField;

public class RSAParamsWindow implements RSAParamsGeneration{

	public JFrame frame;
	
	private RSAParams paramsRSA;
	private Object self = this;
	
	
	private JLabel PLabel, QLabel, NLabel, FiLabel,ELabel,DLabel;
	private JTextField ETStrength;

	
	private boolean generatNow = false;
	/**
	 * Create the application.
	 */
	public RSAParamsWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1061, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblP = new JLabel("P =");
		lblP.setBounds(42, 42, 25, 14);
		frame.getContentPane().add(lblP);
		
		JLabel lblQ = new JLabel("Q =");
		lblQ.setBounds(42, 67, 25, 14);
		frame.getContentPane().add(lblQ);
		
		JLabel lblN = new JLabel("N =");
		lblN.setBounds(42, 91, 25, 14);
		frame.getContentPane().add(lblN);
		
		JLabel lblFi = new JLabel("Fi =");
		lblFi.setBounds(42, 116, 25, 14);
		frame.getContentPane().add(lblFi);
		
		JLabel lblE = new JLabel("E =");
		lblE.setBounds(42, 141, 25, 14);
		frame.getContentPane().add(lblE);
		
		JLabel lblD = new JLabel("D =");
		lblD.setBounds(42, 166, 25, 14);
		frame.getContentPane().add(lblD);
		
		PLabel = new JLabel("notSet");
		PLabel.setBounds(75, 42, 960, 14);
		frame.getContentPane().add(PLabel);
		
		QLabel = new JLabel("notSet");
		QLabel.setBounds(75, 67, 960, 14);
		frame.getContentPane().add(QLabel);
		
		NLabel = new JLabel("notSet");
		NLabel.setBounds(75, 91, 960, 14);
		frame.getContentPane().add(NLabel);
		
		FiLabel = new JLabel("notSet");
		FiLabel.setBounds(75, 116, 960, 14);
		frame.getContentPane().add(FiLabel);
		
		ELabel = new JLabel("notSet");
		ELabel.setBounds(75, 141, 960, 14);
		frame.getContentPane().add(ELabel);
		
		DLabel = new JLabel("notSet");
		DLabel.setBounds(77, 166, 960, 14);
		frame.getContentPane().add(DLabel);
		
		JButton button = new JButton("\u0421\u0433\u0435\u043D\u0435\u0440\u0438\u0440\u043E\u0432\u0430\u0442\u044C");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if(!generatNow)
				{
					generatNow = true;
					Thread generationPSAParams = new Thread(new Runnable() {
						
						@Override
						public void run() {
							paramsRSA = new RSAParams(Integer.parseInt(ETStrength.getText()), (RSAParamsGeneration) self);
							paramsRSA.generete();
						}
					});
					generationPSAParams.setPriority(Thread.MAX_PRIORITY);
					generationPSAParams.start();
				}
			}
		});
		button.setBounds(178, 10, 130, 23);
		frame.getContentPane().add(button);
		
		ETStrength = new JTextField();
		ETStrength.setText("256");
		ETStrength.setBounds(111, 11, 41, 20);
		frame.getContentPane().add(ETStrength);
		ETStrength.setColumns(10);
		
		JLabel lblP_1 = new JLabel("\u0414\u043B\u0438\u043D\u0430 P \u0438 Q");
		lblP_1.setBounds(31, 14, 70, 14);
		frame.getContentPane().add(lblP_1);
	}

	@Override
	public void OnPGenComplete() {
		// TODO Auto-generated method stub
		PLabel.setText(paramsRSA.P.toString());
	}

	@Override
	public void OnQGenComplete() {
		// TODO Auto-generated method stub
		QLabel.setText(paramsRSA.Q.toString());
	}

	@Override
	public void OnNGenComplete() {
		// TODO Auto-generated method stub
		NLabel.setText(paramsRSA.N.toString());
	}

	@Override
	public void OnFiGenComplete() {
		// TODO Auto-generated method stub
		FiLabel.setText(paramsRSA.Fi.toString());
	}

	@Override
	public void OnEGenComplete() {
		// TODO Auto-generated method stub
		ELabel.setText(paramsRSA.E.toString());
	}

	@Override
	public void OnDGenComplete() {
		// TODO Auto-generated method stub
		DLabel.setText(paramsRSA.D.toString());
		generatNow = false;
	}

}
