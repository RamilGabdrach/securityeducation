package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JSeparator;

public class StartWindow {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					StartWindow window = new StartWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public StartWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnRsaparametrs = new JButton("RSAParametrs");
		btnRsaparametrs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				RSAParamsWindow window = new RSAParamsWindow();
				window.frame.setVisible(true);
			}
		});
		btnRsaparametrs.setBounds(133, 30, 169, 23);
		frame.getContentPane().add(btnRsaparametrs);
		
		JButton button = new JButton("\u0428\u0438\u0444\u0440\u043E\u0432\u0430\u043D\u0438\u0435/\u0440\u0430\u0441\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u043D\u0438\u0435");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RSAWorkWindow window = new RSAWorkWindow();
				window.frame.setVisible(true);
			}
		});
		button.setBounds(118, 64, 211, 23);
		frame.getContentPane().add(button);
		
		JButton btnRsa = new JButton("\u0412\u0437\u043B\u043E\u043C RSA");
		btnRsa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				HackRSAWindow window = new HackRSAWindow();
				window.frame.setVisible(true);
			}
		});
		btnRsa.setBounds(133, 200, 169, 23);
		frame.getContentPane().add(btnRsa);
	}
}
