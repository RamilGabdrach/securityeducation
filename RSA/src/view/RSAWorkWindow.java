package view;

import interfaces.RSAParamsGeneration;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import utils.RSA;
import utils.RSAParams;

public class RSAWorkWindow implements RSAParamsGeneration{

	public JFrame frame;
	private JTextField TFMessage;
	private JTextField TFEncryptMessage;
	private JTextField TFDecMessage;

	
	
	private boolean generatNow = false;
	private RSAParams paramsRSA;
	private Object self = this;
	private JTextField ETStrength;
	/**
	 * Create the application.
	 */
	public RSAWorkWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 450, 426);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel label = new JLabel("\u0412\u0432\u0435\u0434\u0438\u0442\u0435 \u0441\u043E\u043E\u0431\u0449\u0435\u043D\u0438\u0435, \u043A\u043E\u0442\u043E\u0440\u043E\u0435 \u043D\u0443\u0436\u043D\u043E \u0437\u0430\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u0442\u044C");
		label.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label.setBounds(21, 11, 391, 39);
		frame.getContentPane().add(label);
		
		TFMessage = new JTextField();
		TFMessage.setBounds(21, 53, 391, 39);
		frame.getContentPane().add(TFMessage);
		TFMessage.setColumns(10);
		
		JLabel label_1 = new JLabel("\u0417\u0430\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u043D\u043D\u043E\u0435 \u0441\u043E\u043E\u0431\u0449\u0435\u043D\u0438\u0435:");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label_1.setBounds(101, 137, 227, 39);
		frame.getContentPane().add(label_1);
		
		JButton btnNewButton = new JButton("\u0417\u0430\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u0442\u044C");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!generatNow)
				{
					generatNow = true;
					Thread generationPSAParams = new Thread(new Runnable() {
						
						@Override
						public void run() {
							paramsRSA = new RSAParams(Integer.parseInt(ETStrength.getText()), (RSAParamsGeneration) self);
							paramsRSA.generete();
						}
					});
					generationPSAParams.setPriority(Thread.MAX_PRIORITY);
					generationPSAParams.start();
				}
			}
		});
		btnNewButton.setBounds(240, 103, 159, 23);
		frame.getContentPane().add(btnNewButton);
		
		TFEncryptMessage = new JTextField();
		TFEncryptMessage.setColumns(10);
		TFEncryptMessage.setBounds(21, 181, 391, 39);
		frame.getContentPane().add(TFEncryptMessage);
		
		JButton button = new JButton("\u0420\u0430\u0441\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u0442\u044C");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!generatNow)
				{
					String decMessage = RSA.decode(paramsRSA, TFEncryptMessage.getText());
					
					TFDecMessage.setText(decMessage);
				}
			}
		});
		button.setBounds(128, 231, 159, 23);
		frame.getContentPane().add(button);
		
		TFDecMessage = new JTextField();
		TFDecMessage.setColumns(10);
		TFDecMessage.setBounds(21, 337, 391, 39);
		frame.getContentPane().add(TFDecMessage);
		
		JLabel label_2 = new JLabel("\u0420\u0430\u0441\u0448\u0438\u0444\u0440\u043E\u0432\u0430\u043D\u043D\u043E\u0435 \u0441\u043E\u043E\u0431\u0449\u0435\u043D\u0438\u0435:");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		label_2.setBounds(83, 287, 263, 39);
		frame.getContentPane().add(label_2);
		
		JLabel label_3 = new JLabel("\u0414\u043B\u0438\u043D\u0430 \u043A\u043B\u044E\u0447\u0430:");
		label_3.setBounds(21, 107, 72, 14);
		frame.getContentPane().add(label_3);
		
		ETStrength = new JTextField();
		ETStrength.setText("128");
		ETStrength.setBounds(102, 103, 86, 20);
		frame.getContentPane().add(ETStrength);
		ETStrength.setColumns(10);
	}

	@Override
	public void OnPGenComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnQGenComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnNGenComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnFiGenComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnEGenComplete() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void OnDGenComplete() {
		generatNow = false;
		
		String encMessage = RSA.encrypt(paramsRSA, TFMessage.getText());
		
		TFEncryptMessage.setText(encMessage);
	}
}
