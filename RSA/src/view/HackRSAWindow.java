package view;

import hack.HackRSA;
import hack.HackingStatus;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.math.BigInteger;

public class HackRSAWindow implements HackingStatus{

	public JFrame frame;
	private JTextField TFEncMessage;
	private JTextField TFN;
	private JTextField TFE;
	JLabel LTime, LP, LQ, LD, LHackMes;
	
	private boolean hackNow = false;
	private HackRSAWindow self = this;

	/**
	 * Create the application.
	 */
	public HackRSAWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 494, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMessage = new JLabel("Message");
		lblMessage.setBounds(10, 11, 46, 14);
		frame.getContentPane().add(lblMessage);
		
		TFEncMessage = new JTextField();
		TFEncMessage.setText("26995737547564296500405026625");
		TFEncMessage.setBounds(66, 8, 265, 20);
		frame.getContentPane().add(TFEncMessage);
		TFEncMessage.setColumns(10);
		
		JLabel lblN = new JLabel("N");
		lblN.setBounds(10, 42, 46, 14);
		frame.getContentPane().add(lblN);
		
		TFN = new JTextField();
		TFN.setText("537403628975761650764156954993");
		TFN.setColumns(10);
		TFN.setBounds(66, 39, 265, 20);
		frame.getContentPane().add(TFN);
		
		JLabel lblE = new JLabel("E");
		lblE.setBounds(10, 72, 46, 14);
		frame.getContentPane().add(lblE);
		
		TFE = new JTextField();
		TFE.setText("517214694290847882761450046499");
		TFE.setColumns(10);
		TFE.setBounds(66, 69, 265, 20);
		frame.getContentPane().add(TFE);
		
		JButton button = new JButton("\u0412\u0437\u043B\u043E\u043C\u0430\u0442\u044C");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(!hackNow)
				{
					hackNow = true;
					BigInteger N = new BigInteger(TFN.getText());
					BigInteger E = new BigInteger(TFE.getText());
					String mes = TFEncMessage.getText();
					HackRSA h= new HackRSA(); 
					h.execute(N, E, mes, self);
				}
			}
		});
		button.setBounds(355, 38, 99, 23);
		frame.getContentPane().add(button);
		
		JLabel label = new JLabel("\u0412\u0440\u0435\u043C\u044F(\u0441\u0435\u043A):");
		label.setBounds(10, 128, 71, 14);
		frame.getContentPane().add(label);
		
		LTime = new JLabel("0");
		LTime.setBounds(91, 128, 340, 14);
		frame.getContentPane().add(LTime);
		
		JLabel lblP = new JLabel("P=");
		lblP.setBounds(10, 153, 64, 14);
		frame.getContentPane().add(lblP);
		
		LP = new JLabel("0");
		LP.setBounds(91, 153, 340, 14);
		frame.getContentPane().add(LP);
		
		JLabel lblQ = new JLabel("Q=");
		lblQ.setBounds(10, 174, 64, 14);
		frame.getContentPane().add(lblQ);
		
		LQ = new JLabel("0");
		LQ.setBounds(91, 174, 340, 14);
		frame.getContentPane().add(LQ);
		
		JLabel lblD = new JLabel("D=");
		lblD.setBounds(10, 199, 64, 14);
		frame.getContentPane().add(lblD);
		
		LD = new JLabel("0");
		LD.setBounds(91, 199, 340, 14);
		frame.getContentPane().add(LD);
		
		JLabel lblMessage_1 = new JLabel("Message:");
		lblMessage_1.setBounds(10, 224, 64, 14);
		frame.getContentPane().add(lblMessage_1);
		
		LHackMes = new JLabel("0");
		LHackMes.setBounds(91, 224, 340, 14);
		frame.getContentPane().add(LHackMes);
	}

	@Override
	public void OnHackComplete(double time, BigInteger P, BigInteger Q,BigInteger D, String message) {
		LTime.setText("" + time);
		LP.setText(P.toString());
		LQ.setText(Q.toString());
		LD.setText(D.toString());
		
		LHackMes.setText(message);
	}
}
