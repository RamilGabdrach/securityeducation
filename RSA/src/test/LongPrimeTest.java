package test;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Test;

import parallel.utils.ParallelPrimeNumber;
import utils.PrimeNumber;

public class LongPrimeTest {

	/*
	
	@Test
	public void test01()
	{
		Thread a = new Thread(new Runnable() {
			
			@Override
			public void run() {
				long beginTime = System.currentTimeMillis();
				BigInteger prime = PrimeNumber.gen512PrimeNumber();
				//Number prime = PrimeNumber.getSimple(128);
				
				double time = (System.currentTimeMillis() - beginTime)/1000d;
				
				
				System.out.println("I = A");
				System.out.println("A prime number = " + prime); 
				System.out.println("A length " + prime.bitCount() +" define time " + time + " sec.");
				
			}
		});
		
		Thread b = new Thread(new Runnable() {
			
			@Override
			public void run() {
				long beginTime = System.currentTimeMillis();
				BigInteger prime = PrimeNumber.gen512PrimeNumber();
				//Number prime = PrimeNumber.getSimple(128);
				
				double time = (System.currentTimeMillis() - beginTime)/1000d;
				
				
				System.out.println("I = B");
				System.out.println("B prime number = " + prime); 
				System.out.println("B length " + prime.bitCount() +" define time " + time + " sec.");
				
			}
		});
		
		
		a.setPriority(Thread.MAX_PRIORITY);
		b.setPriority(Thread.MAX_PRIORITY);
		a.start();
		b.start();
		
	}*/
	
	
	
	
	
	@Test
	public void test01() {
		
		long ollBeginTime = System.currentTimeMillis();
		
		double maxTime = 0;
		double minTime = 1000000;
		
		int k = 50;
		
		for(int i =0; i < k; i++)
		{
			long beginTime = System.currentTimeMillis();
			BigInteger prime = PrimeNumber.gen512PrimeNumber();
			//Number prime = PrimeNumber.getSimple(128);
			
			double time = (System.currentTimeMillis() - beginTime)/1000d;
			
			maxTime = Math.max(time, maxTime);
			minTime = Math.min(time, minTime);
			
			System.out.println("I = " + i);
			System.out.println("prime number = " + prime); 
			System.out.println("length " + prime.bitCount() +" define time " + time + " sec.");
		}
		
		
		double time = (System.currentTimeMillis() - ollBeginTime)/1000d;
		System.out.println("oll time = " + time);
		System.out.println("Max time = " + maxTime);
		System.out.println("Min time = " + minTime);
		System.out.println("    time = " + time/k);
		
		
	}
	
}
