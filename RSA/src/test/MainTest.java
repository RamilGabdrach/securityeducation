package test;

import java.math.BigInteger;

import parallel.utils.ParallelPrimeNumber;
import interfaces.PrimeDefindListner;


public class MainTest implements PrimeDefindListner{
	
	
	static double maxTime = 0;
	static double minTime = 1000000;
	
	static long beginTime;
	static int i;
	static int k = 50;
	static long ollBeginTime;
	public  void main()
	{

		ollBeginTime = System.currentTimeMillis();
		
		beginTime = System.currentTimeMillis();
	
		ParallelPrimeNumber p = new ParallelPrimeNumber();
		p.generatePrime(512, this);		
	}

	@Override
	public void OnDefind(BigInteger prime) {
		
		
		
		double time = (System.currentTimeMillis() - beginTime)/1000d;
		maxTime = Math.max(time, maxTime);
		minTime = Math.min(time, minTime);
		
		System.out.println("I = " + i);
		System.out.println("prime number = " + prime); 
		System.out.println("length " + prime.bitCount() +" define time " + time + " sec.");
		
		i++;
		if(i < k)
		{
			beginTime = System.currentTimeMillis();
			ParallelPrimeNumber p = new ParallelPrimeNumber();
			p.generatePrime(512, this);
		}
		else
		{
			time = (System.currentTimeMillis() - ollBeginTime)/1000d;
			System.out.println("oll time = " + time);
			System.out.println("Max time = " + maxTime);
			System.out.println("Min time = " + minTime);
			System.out.println("    time = " + time/k);
		}
		
	}
		
}
